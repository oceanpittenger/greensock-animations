(function ($, IBM) {
    window.scene1tl = new TimelineMax();
    window.scene2tl = new TimelineMax();
    window.scene3tl = new TimelineMax();
    window.scene4tl = new TimelineMax();
    window.scene5tl = new TimelineMax();
    window.scene6tl = new TimelineMax();
    window.scene7tl = new TimelineMax();
    window.scene8tl = new TimelineMax();
    window.scene9tl = new TimelineMax();

    var app = {

        init: function(){
            app.scene1StartInit();
            app.scene1CollapseInit();
            app.scene2AnimationInit();
            app.scene2TextInit();
            app.scene3AnimationInit();
            app.scene3TextInit();
            app.scene4AnimationInit();
            app.scene4TextInit();
            app.scene5AnimationInit();
            app.scene5AnimationInit2();
            app.scene5AnimationInit3();
            app.scene5Text1Init();
            app.scene5Text2Init();
            app.scene5Text3Init();
            app.scene6AnimationInit();
            app.scene6TextInit();
            app.scene7AnimationInit();
            app.scene7AnimationInit2();
            app.scene7AnimationInit3();
            app.scene7TextInit();
            app.scene7Text2Init();
            app.scene7Text3Init();
            app.scene8AnimationInit();
            app.scene8AnimationInit2();
            app.scene8AnimationInit3();
            app.scene8AnimationInit4();
            app.scene8TextInit();
            app.scene9AnimationInit();
            app.scene9TextInit();
            app.scene1LinkInit();
            app.scene2LinkInit();
            app.scene3LinkInit();
            app.scene4LinkInit();
            app.scene5LinkInit();
            app.scene6LinkInit();
            app.scene7LinkInit();
            app.scene8LinkInit();
            app.scene9LinkInit();
            app.startOverInit();
        },

        //*****Init functions*****//

        scene1StartInit: function(){
            $("#scene-1-start").on("click", app.handleScene1GetStartedButtonClick);
        },
        scene1CollapseInit: function(){
            //app.handleScene1Collapse
            $('#scene-1 .next-btn').on('click', app.handleScene1Collapse);
        },
        scene2TextInit: function(){
            $('#scene-2-clickpoint').on('click', app.handleScene2Text);
        },
        scene2AnimationInit: function(){
            //app.handleScene3Animation
            $('#scene-2 .next-btn-screen2').on('click', app.handleScene3Animation);
        },
        scene3AnimationInit: function(){
            //app.handleScene3Animation
            $('#scene-3 .next-btn-screen3').on('click', app.handleScene4Animation);
        },
        scene3TextInit: function(){
            $('#scene-3-clickpoint').on('click', app.handleScene3Text);
        },
        scene4AnimationInit: function(){
            //app.handleScene4Animation
            $('#scene-4 .next-btn-screen4').on('click', app.handleScene5Animation);
        },
        scene4TextInit: function(){
            $('#scene-4-clickpoint').on('click', app.handleScene4Text);
        },
        scene5AnimationInit: function(){
            //app.handleScene5Animation
            $('#scene-5 .next-btn-screen5-1').on('click', app.handleScene5Next);
        },
        scene5AnimationInit2: function(){
            //app.handleScene5Animation
            $('#scene-5 .next-btn-screen5-2').on('click', app.handleScene5Next2);
        },
        scene5AnimationInit3: function(){
            //app.handleScene5Animation
            $('#scene-5 .next-btn-screen5-3').on('click', app.handleScene6Animation);
        },
        scene5Text1Init: function(){
            $('#scene-5-clickpoint').on('click', app.handleScene5Text);
        },
        scene5Text2Init: function(){
            $('#scene-5-clickpoint-2').on('click', app.handleScene5Text2);
        },
        scene5Text3Init: function(){
            $('#scene-5-clickpoint-3').on('click', app.handleScene5Text3);
        },
        scene6AnimationInit: function(){
            //app.handleScene6Animation
            $('#scene-6 .next-btn-screen6').on('click', app.handleScene7Animation);
        },
        scene6TextInit: function(){
            $('#scene-6-clickpoint').on('click', app.handleScene6Text);
        },
        scene7AnimationInit: function(){
            //app.handleScene7Animation
            $('#scene-7 .next-btn-screen7-1').on('click', app.handleScene7Next);
        },
        scene7AnimationInit2: function(){
            //app.handleScene5Animation
            $('#scene-7 .next-btn-screen7-2').on('click', app.handleScene7Next2);
        },
        scene7AnimationInit3: function(){
            //app.handleScene5Animation
            $('#scene-7 .next-btn-screen7-3').on('click', app.handleScene8Animation);
        },
        scene7TextInit: function(){
            $('#scene-7-clickpoint').on('click', app.handleScene7Text);
        },
        scene7Text2Init: function(){
            $('#scene-7-clickpoint-2').on('click', app.handleScene7Text2);
        },
        scene7Text3Init: function(){
            $('#scene-7-clickpoint-3').on('click', app.handleScene7Text3);
        },
        scene8AnimationInit: function(){
            //app.handleScene8Animation
            $('#scene-8 .next-btn-screen8-1').on('click', app.handleScene8Next);
        },
        scene8AnimationInit2: function(){
            //app.handleScene8Animation
            $('#scene-8 .next-btn-screen8-2').on('click', app.handleScene8Next2);
        },
        scene8AnimationInit3: function(){
            //app.handleScene8Animation
            $('#scene-8 .next-btn-screen8-3').on('click', app.handleScene8Next3);
        },scene8AnimationInit4: function(){
            //app.handleScene8Animation
            $('#scene-8 .next-btn-screen8-4').on('click', app.handleScene8Next4);
        },
        scene8TextInit: function(){
            $('#scene-8-clickpoint').on('click', app.handleScene8Text);
        },
        scene9AnimationInit: function(){
            //app.handleScene9Animation
            $('#scene-8 .next-btn-screen8-5').on('click', app.handleScene9Animation);
        },
        scene9TextInit: function(){
            $('#scene-9-clickpoint').on('click', app.handleScene9Text);
        },
        
        //handle progress dots links

        scene1LinkInit: function(){
           $('.scene-1-btn').on('click', app.handleScene1Link);
        },
        scene2LinkInit: function(){
           $('.scene-2-btn').on('click', app.handleScene2Link);
        },
        scene3LinkInit: function(){
           $('.scene-3-btn').on('click', app.handleScene3Link);
        },
        scene4LinkInit: function(){
           $('.scene-4-btn').on('click', app.handleScene4Link);
        },
        scene5LinkInit: function(){
           $('.scene-5-btn').on('click', app.handleScene5Link);
        },
        scene6LinkInit: function(){
           $('.scene-6-btn').on('click', app.handleScene6Link);
        },
        scene7LinkInit: function(){
           $('.scene-7-btn').on('click', app.handleScene7Link);
        },
        scene8LinkInit: function(){
           $('.scene-8-btn').on('click', app.handleScene8Link);
        },
        scene9LinkInit: function(){
           $('.scene-9-btn').on('click', app.handleScene9Link);
        },

        //Start over button//
        startOverInit: function(){
            $('.start-over-btn').on('click', app.handleStartOverBtn);
        },

        //functions to define animations
        handleScene1GetStartedButtonClick: function(e){
            //Animate button & line off screen
            TweenLite.to($('#scene-1-start'), 2.1, {y: 1000, ease: Power4.easeInOut});
            TweenLite.to($('#scene1-dotted-line'), 2, {y: 1000, ease: Power4.easeInOut});

            //fade out text 1 & fade in text 2
            $( "#scene1-text" ).fadeOut(1000, function(){
                $( "#scene1-text2" ).fadeIn(1000);
            });
        
            e.preventDefault();
            e.stopPropagation();
        },
        /*handleCloseTextButton: function(e){
            $('#scene1-text2').fadeOut(1000);
            $('#scene1-text2').fadeOut(1000, function(){
                $("#scene1-text").fadeIn(1000);
            });

        },*/
        handleScene1Collapse: function(e){
            //animate scene out

            $('#scene1-text2').fadeOut(600);

            scene1tl.fromTo($('#scene-1 #Mountain_1_'), 2, {y: 0}, {y: 1000, ease: Power1.easeInOut})
            .fromTo($('#scene-1 #BG_x5F_buildings'), 2, {y: 0}, {y: 1000, ease: Power1.easeInOut}, '-=1.8')
            .fromTo($('#scene-1 #AirTram'), 2, {y: 0}, {y: 1000, ease: Power1.easeInOut}, '-=1.8')
            .fromTo($('#scene-1 #BuildingsAboveAirTram'), 2, {y: 0}, {y: 1000, ease: Power1.easeInOut}, '-=1.8')
            .fromTo($('#scene-1 #Trees_1_'), 2, {y: 0}, {y: 1000, ease: Power1.easeInOut}, '-=1.8')
            .fromTo($('#scene-1 #Ground'), 2, {y: 0}, {y: 1000, ease: Power1.easeInOut}, '-=1.8');

            $('#scene-1 .progress-dots').hide();

            app.handleScene2Animation();

            $('#scene-2').show();

            e.preventDefault();
            e.stopPropagation();
        },
        handleScene2Animation: function(e){

            $('#scene-1').delay(500).hide(1000);
            $('#scene-2').show();
            $('#start-over-scene2').fadeIn(1000);

            scene2tl.fromTo($('#scene-2 #Cloud_1_'), 2, {y: 1000} , {y: 0, ease: Power1.easeInOut})
            .fromTo($('#scene-2 #Buildings'), 2, {y: 1000}, {y: 0, ease: Power1.easeInOut}, '-=1.8')
            .fromTo($('#scene-2 #scene-2-clickpoint'), 2, {y: 1000}, {y: 0, ease: Power1.easeInOut}, '-=1.8')
            .fromTo($('#scene-2 #scene-2-dotted-line'), 2, {y: 1000}, {y: 0, ease: Power1.easeInOut}, '-=1.8')
            .fromTo($('#scene-2 #Trees_1_'), 2, {y: 1000}, {y: 0, ease: Power1.easeInOut}, '-=1.8')
            .fromTo($('#scene-2 #Ground'), 2, {y: 1000}, {y: 0, ease: Power1.easeInOut}, '-=1.8')
            .fromTo($('#scene-2 #Bill'), 2, {y: 1000}, {y: 0, ease: Power1.easeInOut,}, '-=1.8')
            .fromTo($('#scene-2 #Guy_x5F_on_x5F_SkateBoard'), 2, {x: -1000},{x: 0, ease: Power1.easeInOut}, '-=1.8')

            e.preventDefault();
            e.stopPropagation();

        },
        handleScene2Text: function(e){
            $( "#scene2-text" ).fadeIn(1000);
            //Animate button & line off screen
            TweenLite.to($('#scene-2-clickpoint'), 2.1, {y: 1000, ease: Power4.easeInOut});
            TweenLite.to($('#scene-2-dotted-line'), 2, {y: 1000, ease: Power4.easeInOut});

            e.preventDefault();
            e.stopPropagation();
        },
        handleScene3Animation: function(e){
            scene2tl.reverse();
            $('#scene2-text').fadeOut(600);
            $('#scene-2').delay(500).hide(1000); 
            $('#scene-3').show();
            $('#start-over-scene3').fadeIn(1000);

            scene3tl.fromTo($('#scene-3 #BG_x5F_building'), 2, {y: 1000}, {y: 0, ease: Power1.easeInOut})
            .fromTo($('#scene-3 #scene-3-clickpoint'), 2, {y: 1000}, {y: 0, ease: Power1.easeInOut}, '-=1.8')
            .fromTo($('#scene-3 #scene-3-dotted-line'), 2, {y: 1000}, {y: 0, ease: Power1.easeInOut}, '-=1.8')
            .fromTo($('#scene-3 #Land'), 2, {y: 1000}, {y: 0, ease: Power1.easeInOut}, '-=1.8')
            .fromTo($('#scene-3 #Bus_1_'), 2, {y: 1000}, {y: 0, ease: Power1.easeInOut}, '-=1.8')
            .fromTo($('#scene-3 #BusStop_1_'), 2, {y: 1000}, {y: 0, ease: Power1.easeInOut}, '-=1.8')
            .fromTo($('#scene-3 #Bill_1_'), 2, {y: 1000}, {y: 0, ease: Power1.easeInOut,}, '-=1.8')
            .fromTo($('#scene-3 #Couple_1_'), 2, {x: 1000},{x: 0, ease: Power1.easeInOut}, '-=1.8')

            e.preventDefault();
            e.stopPropagation();
        },
        handleScene3Text: function(e){
            $( "#scene3-text" ).fadeIn(1000);
            //Animate button & line off screen
            TweenLite.to($('#scene-3-clickpoint'), 2.1, {y: 1000, ease: Power4.easeInOut});
            TweenLite.to($('#scene-3-dotted-line'), 2, {y: 1000, ease: Power4.easeInOut});

            e.preventDefault();
            e.stopPropagation();
        },
        handleScene4Animation: function(e){
            scene3tl.reverse();
            $('#scene3-text').fadeOut(600);
            $('#scene-3').delay(500).hide(1000); 
            $('#scene-4').show();
            $('#start-over-scene4').fadeIn(1000);

            scene4tl.fromTo($('#scene-4 #BusSide'), 2, {y: 1000}, {y: 0, ease: Power1.easeInOut})
            .fromTo($('#scene-4 #scene-4-clickpoint'), 2, {y: 1000}, {y: 0, ease: Power1.easeInOut}, '-=1.8')
            .fromTo($('#scene-4 #scene-4-dotted-line'), 2, {y: 1000}, {y: 0, ease: Power1.easeInOut}, '-=1.8')    
            .fromTo($('#scene-4 #BusSeatLeft'), 2, {y: 1000}, {y: 0, ease: Power1.easeInOut}, '-=1.8')
            .fromTo($('#scene-4 #LadySitting'), 2, {y: 1000}, {y: 0, ease: Power1.easeInOut}, '-=1.8')            
            .fromTo($('#scene-4 #BusSeatRight'), 2, {y: 1000}, {y: 0, ease: Power1.easeInOut}, '-=1.8')
            .fromTo($('#scene-4 #Bill_1_'), 2, {y: 1000}, {y: 0, ease: Power1.easeInOut,}, '-=1.8')
            .fromTo($('#scene-4 #WorkOutGirl'), 2, {x: 1000},{x: 0, ease: Power1.easeInOut}, '-=1.8')

            e.preventDefault();
            e.stopPropagation();
        },
        handleScene4Text: function(e){
            $( "#scene4-text" ).fadeIn(1000);
            //Animate button & line off screen
            TweenLite.to($('#scene-4-clickpoint'), 2.1, {y: 1000, ease: Power4.easeInOut});
            TweenLite.to($('#scene-4-dotted-line'), 2, {y: 1000, ease: Power4.easeInOut});

            e.preventDefault();
            e.stopPropagation();
        },
        handleScene5Animation: function(e){
            scene4tl.reverse();
            $('#scene4-text').fadeOut(600);
            $('#scene-4').delay(500).hide(1000); 
            $('#scene-5').show();
            $('#start-over-scene5').fadeIn(1000);

            scene5tl.fromTo($('#scene-5 #Light_1_'), 2, {y: 1000}, {y: 0, ease: Power1.easeInOut})
            .fromTo($('#scene-5 #scene-5-clickpoint'), 2, {y: 1000}, {y: 0, ease: Power1.easeInOut}, '-=1.8')
            .fromTo($('#scene-5 #scene-5-dotted-line'), 2, {y: 1000}, {y: 0, ease: Power1.easeInOut}, '-=1.8')    
            .fromTo($('#scene-5 #Shelf'), 2, {y: 1000}, {y: 0, ease: Power1.easeInOut}, '-=1.8')
            .fromTo($('#scene-5 #Tables'), 2, {y: 1000}, {y: 0, ease: Power1.easeInOut}, '-=1.8')     
            .fromTo($('#scene-5 #Suitcase_1_'), 2, {y: 1000}, {y: 0, ease: Power1.easeInOut}, '-=1.8')
            .fromTo($('#scene-5 #Couch_1_'), 2, {y: 1000}, {y: 0, ease: Power1.easeInOut,}, '-=1.8')

            e.preventDefault();
            e.stopPropagation();

        },
        handleScene5Text: function(e){
            $( "#scene5-text" ).fadeIn(1000);
            //Animate button & line off screen
            TweenLite.to($('#scene-5-clickpoint'), 2.1, {y: 1000, ease: Power4.easeInOut});
            TweenLite.to($('#scene-5-dotted-line'), 2, {y: 1000, ease: Power4.easeInOut});

            e.preventDefault();
            e.stopPropagation();
        },
        handleScene5Next: function(e){
            $( "#scene5-text" ).fadeOut(1000);
            $('#scene-5-clickpoint-2').show();
            $('#scene-5-dotted-line-2').show();

            TweenLite.fromTo($('#scene-5-clickpoint-2'), 2.1,{y: 1000}, {y: 0, ease: Power4.easeInOut});
            TweenLite.fromTo($('#scene-5-dotted-line-2'), 2, {y: 1000}, {y: 0, ease: Power4.easeInOut});

            e.preventDefault();
            e.stopPropagation();
        },
        handleScene5Text2: function(e){
            $( "#scene5-text2" ).fadeIn(1000);
            //Animate button & line off screen
            TweenLite.to($('#scene-5-clickpoint-2'), 2.1, {y: 1000, ease: Power4.easeInOut});
            TweenLite.to($('#scene-5-dotted-line-2'), 2, {y: 1000, ease: Power4.easeInOut});

            e.preventDefault();
            e.stopPropagation();
        },
        handleScene5Next2: function(e){
            $( "#scene5-text2" ).fadeOut(1000);
            $('#scene-5-clickpoint-3').show();
            $('#scene-5-dotted-line-3').show();

            TweenLite.fromTo($('#scene-5-clickpoint-3'), 2.1,{y: 1000}, {y: 0, ease: Power4.easeInOut});
            TweenLite.fromTo($('#scene-5-dotted-line-3'), 2, {y: 1000}, {y: 0, ease: Power4.easeInOut});

            e.preventDefault();
            e.stopPropagation();
        },
        handleScene5Text3: function(e){
            $( "#scene5-text3" ).fadeIn(1000);
            //Animate button & line off screen
            TweenLite.to($('#scene-5-clickpoint-3'), 2.1, {y: 1000, ease: Power4.easeInOut});
            TweenLite.to($('#scene-5-dotted-line-3'), 2, {y: 1000, ease: Power4.easeInOut});

            e.preventDefault();
            e.stopPropagation();
        },
        handleScene6Animation: function(e){
            scene5tl.reverse();
            $('#scene5-text3').fadeOut(600);
            $('#scene-5').delay(500).hide(700); 
            $('#scene-6').show();
            $('#start-over-scene6').fadeIn(1000);

            scene6tl.fromTo($('#scene-6 #TV_x5F_stand'), 2, {y: 1000}, {y: 0, ease: Power1.easeInOut})
            .fromTo($('#scene-6 #scene-6-clickpoint'), 2, {y: 1000}, {y: 0, ease: Power1.easeInOut}, '-=1.8')
            .fromTo($('#scene-6 #scene-6-dotted-line'), 2, {y: 1000}, {y: 0, ease: Power1.easeInOut}, '-=1.8')
            .fromTo($('#scene-6 #Hand_x5F_CellPhone'), 2, {y: 1000}, {y: 0, ease: Power1.easeInOut}, '-=1.8')     
            .fromTo($('#scene-6 #Couch_1_'), 2, {y: 1000}, {y: 0, ease: Power1.easeInOut}, '-=1.8')
            .fromTo($('#scene-6 #Bill'), 2, {y: 1000}, {y: 0, ease: Power1.easeInOut}, '-=1.8')
            .fromTo($('#scene-6 #Suitcase_1_'), 2, {y: 1000}, {y: 0, ease: Power1.easeInOut}, '-=1.8')
            
            e.preventDefault();
            e.stopPropagation();
        },
        handleScene6Text: function(e){
            $( "#scene6-text" ).fadeIn(1000);
            //Animate button & line off screen
            TweenLite.to($('#scene-6-clickpoint'), 2.1, {y: 1000, ease: Power4.easeInOut});
            TweenLite.to($('#scene-6-dotted-line'), 2, {y: 1000, ease: Power4.easeInOut});

            e.preventDefault();
            e.stopPropagation();
        },
        handleScene7Animation: function(e){
            scene6tl.reverse();
            $('#scene6-text').fadeOut(600);
            $('#scene-6').delay(300).hide(500); 
            $('#scene-7').show();

            scene7tl.fromTo($('#scene-7 #Hand_x5F_CellPhone'), 2, {y: 1000}, {y: 0, ease: Power1.easeInOut})           
            .fromTo($('#scene-7 #BottomLine_1_'), 2, {y: 1000}, {y: 0, ease: Power1.easeInOut}, '-=1.8')

            TweenLite.fromTo($('#scene-7 #scene-7-clickpoint'), 2.1, {x: 1000}, {x: 10, ease: Power1.easeInOut})
            TweenLite.fromTo($('#scene-7 #scene-7-dotted-line'), 2.1, {x: 1000}, {x: 8, ease: Power1.easeInOut}) 
            
            e.preventDefault();
            e.stopPropagation();
        },
        handleScene7Text: function(e){
            $( "#scene7-text" ).fadeIn(1000);
            //Animate button & line off screen
            TweenLite.to($('#scene-7-clickpoint'), 2.1, {x: -1000, ease: Power4.easeInOut});
            TweenLite.to($('#scene-7-dotted-line'), 2, {x: -1000, ease: Power4.easeInOut});

            e.preventDefault();
            e.stopPropagation();
        },
        handleScene7Next: function(e){
            $( "#scene7-text" ).fadeOut(1000);
            $('#scene-7-clickpoint-2').delay(500).show(1000);
            $('#scene-7-dotted-line-2').delay(500).show(1000);

            TweenLite.fromTo($('#scene-7-clickpoint-2'), 2.1, {x: 1000}, {x: 0, ease: Power4.easeInOut});
            TweenLite.fromTo($('#scene-7-dotted-line-2'), 2, {x: 1000},  {x: 0, ease: Power4.easeInOut});

            e.preventDefault();
            e.stopPropagation();
        },
        handleScene7Text2: function(e){
            $( "#scene7-text2" ).fadeIn(1000);
            //Animate button & line off screen
            TweenLite.to($('#scene-7-clickpoint-2'), 2.1, {x: -1000, ease: Power4.easeInOut});
            TweenLite.to($('#scene-7-dotted-line-2'), 2, {x: -1000, ease: Power4.easeInOut});

            e.preventDefault();
            e.stopPropagation();
        },
        handleScene7Next2: function(e){
            $( "#scene7-text2" ).fadeOut(1000);
            $('#scene-7-clickpoint-3').show();
            $('#scene-7-dotted-line-3').show();

            TweenLite.fromTo($('#scene-7-clickpoint-3'), 2.1, {y: 1000}, {y: 0, ease: Power4.easeInOut});
            TweenLite.fromTo($('#scene-7-dotted-line-3'), 2,  {y: 1000}, {y: 0, ease: Power4.easeInOut});

            e.preventDefault();
            e.stopPropagation();
        },
        handleScene7Text3: function(e){
            $( "#scene7-text3" ).fadeIn(1000);
            //Animate button & line off screen
            TweenLite.to($('#scene-7-clickpoint-3'), 2.1, {y: 1000, ease: Power4.easeInOut});
            TweenLite.to($('#scene-7-dotted-line-3'), 2, {y: 1000, ease: Power4.easeInOut});

            e.preventDefault();
            e.stopPropagation();
        },
        handleScene8Animation: function(e){
            scene7tl.reverse();
            $('#scene7-text3').fadeOut(600);
            $('#scene-7').delay(500).hide(700); 
            $('#scene-8').show();
            $('#start-over-scene8').fadeIn(1000);

            scene8tl.fromTo($('#scene-8 #Lights'), 2, {y: 1000}, {y: 0, ease: Power1.easeInOut})
            .fromTo($('#scene-8 #scene-8-clickpoint'), 2, {y: 1000}, {y: 0, ease: Power1.easeInOut}, '-=1.8')
            .fromTo($('#scene-8 #scene-8-dotted-line'), 2, {y: 1000}, {y: 0, ease: Power1.easeInOut}, '-=1.8')    
            .fromTo($('#scene-8 #Land'), 2, {y: 1000}, {y: 0, ease: Power1.easeInOut}, '-=1.8')
            .fromTo($('#scene-8 #Floor'), 2, {y: 1000}, {y: 0, ease: Power1.easeInOut}, '-=1.8')
            .fromTo($('#scene-8 #TableLegs'), 2, {y: 1000}, {y: 0, ease: Power1.easeInOut}, '-=1.8')
            .fromTo($('#scene-8 #Left_x5F_Window'), 2, {y: 1000}, {y: 0, ease: Power1.easeInOut}, '-=1.8')
            .fromTo($('#scene-8 #Left_x5F_TopTable'), 2, {y: 1000}, {y: 0, ease: Power1.easeInOut}, '-=1.8')
            .fromTo($('#scene-8 #Left_x5F_Bill'), 2, {y: 1000}, {y: 0, ease: Power1.easeInOut}, '-=1.8')
            .fromTo($('#scene-8 #Suitcase'), 2, {y: 1000}, {y: 0, ease: Power1.easeInOut}, '-=1.8')
            .fromTo($('#scene-8 #RightWindow'), 2, {y: 1000}, {y: 0, ease: Power1.easeInOut}, '-=1.8')
            .fromTo($('#scene-8 #Right_x5F_Hacker_x5F_01'), 2, {y: 1000}, {y: 0, ease: Power1.easeInOut,}, '-=1.8')
            .fromTo($('#scene-8 #Right_x5F_TopTable'), 2, {y: 1000}, {y: 0, ease: Power1.easeInOut}, '-=1.8')
            .fromTo($('#scene-8 #GFX_x5F_on_x5F_Screen_x5F_01'), 2, {y: 1000}, {y: 0, ease: Power1.easeInOut}, '-=1.8')
            
            e.preventDefault();
            e.stopPropagation();
        },
        handleScene8Text: function(e){
            $( "#scene8-text" ).fadeIn(1000);
            //Animate button & line off screen
            TweenLite.to($('#scene-8-clickpoint'), 2.1, {y: 1000, ease: Power4.easeInOut});
            TweenLite.to($('#scene-8-dotted-line'), 2, {y: 1000, ease: Power4.easeInOut});

            e.preventDefault();
            e.stopPropagation();
        },
        handleScene8Next: function(e){
            $( "#scene8-text" ).fadeOut(1000, function(){
                $( "#scene8-text2" ).fadeIn(1000);
            });
            $('#scene-8 #Right_x5F_Hacker_x5F_02').show();
            $('#scene-8 #GFX_x5F_on_x5F_Screen_x5F_02').show();

            TweenLite.to($('#scene-8 #Right_x5F_Hacker_x5F_01'), 2.1, {y: 1000, ease: Power4.easeInOut});
            TweenLite.to($('#scene-8 #GFX_x5F_on_x5F_Screen_x5F_01'), 2, {y: 1000, ease: Power4.easeInOut});

            TweenLite.fromTo($('#scene-8 #Right_x5F_Hacker_x5F_02'), 2.2, {y: 1000}, {y: 0, ease: Power1.easeInOut,}, '-=1.8')
            TweenLite.fromTo($('#scene-8 #GFX_x5F_on_x5F_Screen_x5F_02'), 2.2, {y: 1000}, {y: 0, ease: Power1.easeInOut,}, '-=1.8')
            
            e.preventDefault();
            e.stopPropagation();
        },
        handleScene8Next2: function(e){
            $( "#scene8-text2" ).fadeOut(1000, function(){
                $( "#scene8-text3" ).fadeIn(1000);
            });

            $('#scene-8 #Right_x5F_Hacker_x5F_03').show();
            $('#scene-8 #GFX_x5F_on_x5F_Screen_x5F_03').show();

            TweenLite.to($('#scene-8 #Right_x5F_Hacker_x5F_02'), 2.1, {y: 1000, ease: Power4.easeInOut});
            TweenLite.to($('#scene-8 #GFX_x5F_on_x5F_Screen_x5F_02'), 2, {y: 1000, ease: Power4.easeInOut});

            TweenLite.fromTo($('#scene-8 #Right_x5F_Hacker_x5F_03'), 2.2, {y: 1000}, {y: 0, ease: Power1.easeInOut,}, '-=1.8')
            TweenLite.fromTo($('#scene-8 #GFX_x5F_on_x5F_Screen_x5F_03'), 2.2, {y: 1000}, {y: 0, ease: Power1.easeInOut,}, '-=1.8')
            
            e.preventDefault();
            e.stopPropagation();
        },
        handleScene8Next3: function(e){
            $( "#scene8-text3" ).fadeOut(1000, function(){
                $( "#scene8-text4" ).fadeIn(1000);
            });

            $('#scene-8 #Right_x5F_Hacker_x5F_04').show();
            $('#scene-8 #GFX_x5F_on_x5F_Screen_x5F_04').show();

            TweenLite.to($('#scene-8 #Right_x5F_Hacker_x5F_03'), 2.1, {y: 1000, ease: Power4.easeInOut});
            TweenLite.to($('#scene-8 #GFX_x5F_on_x5F_Screen_x5F_03'), 2, {y: 1000, ease: Power4.easeInOut});

            TweenLite.fromTo($('#scene-8 #Right_x5F_Hacker_x5F_04'), 2.2, {y: 1000}, {y: 0, ease: Power1.easeInOut,}, '-=1.8')
            TweenLite.fromTo($('#scene-8 #GFX_x5F_on_x5F_Screen_x5F_04'), 2.2, {y: 1000}, {y: 0, ease: Power1.easeInOut,}, '-=1.8')
            
            e.preventDefault();
            e.stopPropagation();
        },
        handleScene8Next4: function(e){
            $( "#scene8-text4" ).fadeOut(1000, function(){
                $( "#scene8-text5" ).fadeIn(1000);
            });

            $('#scene-8 #Right_x5F_Hacker_x5F_05').show();
            $('#scene-8 #GFX_x5F_on_x5F_Screen_x5F_05').show();

            TweenLite.to($('#scene-8 #Right_x5F_Hacker_x5F_04'), 2.1, {y: 1000, ease: Power4.easeInOut});
            TweenLite.to($('#scene-8 #GFX_x5F_on_x5F_Screen_x5F_04'), 2, {y: 1000, ease: Power4.easeInOut});

            TweenLite.fromTo($('#scene-8 #Right_x5F_Hacker_x5F_05'), 2.2, {y: 1000}, {y: 0, ease: Power1.easeInOut,}, '-=1.8')
            TweenLite.fromTo($('#scene-8 #GFX_x5F_on_x5F_Screen_x5F_05'), 2.2, {y: 1000}, {y: 0, ease: Power1.easeInOut,}, '-=1.8')
            
            e.preventDefault();
            e.stopPropagation();
        },
        handleScene9Animation: function(e){
            scene8tl.reverse();
            $('#scene8-text5').fadeOut(550);
            $('#scene-8').delay(500).hide(600); 
            $('#scene-9').show();
            $('#start-over-scene9').fadeIn(1000);

            scene9tl.fromTo($('#scene-9 #Light'), 2, {y: 1000}, {y: 0, ease: Power1.easeInOut})    
            .fromTo($('#scene-9 #Ground'), 2, {y: 1000}, {y: 0, ease: Power1.easeInOut}, '-=1.8')
            .fromTo($('#scene-9 #Table'), 2, {y: 1000}, {y: 0, ease: Power1.easeInOut}, '-=1.8')
            .fromTo($('#scene-9 #Bill'), 2, {y: 1000}, {y: 0, ease: Power1.easeInOut}, '-=1.8')
            .fromTo($('#scene-9 #RightHacker_1_'), 2, {y: 1000}, {y: 0, ease: Power1.easeInOut}, '-=1.8')
            .fromTo($('#scene-9 #Wall_1_'), 2, {y: 1000}, {y: 0, ease: Power1.easeInOut}, '-=1.8')
            .fromTo($('#scene-9 #scene-9-clickpoint'), 2, {y: 1000}, {y: 0, ease: Power1.easeInOut}, '-=1.8')
            .fromTo($('#scene-9 #scene-9-dotted-line'), 2, {y: 1000}, {y: 0, ease: Power1.easeInOut}, '-=1.8')

            e.preventDefault();
            e.stopPropagation();
        },
        handleScene9Text: function(e){
            $( "#scene9-text" ).fadeIn(1000);
            //Animate button & line off screen
            TweenLite.to($('#scene-9-clickpoint'), 2.1, {y: 1000, ease: Power4.easeInOut});
            TweenLite.to($('#scene-9-dotted-line'), 2, {y: 1000, ease: Power4.easeInOut});

            e.preventDefault();
            e.stopPropagation();
        },

        //functions for dot links
        handleScene1Link: function(e){

            window.location.reload();

            /*$('.scene').fadeOut(1000);
            $('#scene-1').show();
            $('#scene1-text').fadeIn(2500);
            $('#scene-1 .progress-dots').show();

            var tl = new TimelineMax();

            tl.fromTo($('#scene-1-start'), 2, {y: 1000} ,{y: 0, ease: Power4.easeInOut})
            .fromTo($('#scene1-dotted-line'), 2, {y: 1000}, {y: 10, ease: Power1.easeInOut}, '-=1.8')
            .fromTo($('#scene-1 #Mountain_1_'), 2, {y: 1000}, {y: 0, ease: Power1.easeInOut}, '-=1.8')
            .fromTo($('#scene-1 #BG_x5F_buildings'), 2, {y: 1000}, {y: 10, ease: Power1.easeInOut}, '-=1.8')
            .fromTo($('#scene-1 #AirTram'), 2, {y: 1000}, {y: 0, ease: Power1.easeInOut}, '-=1.8')
            .fromTo($('#scene-1 #BuildingsAboveAirTram'), 2, {y: 1000}, {y: 0, ease: Power1.easeInOut}, '-=1.8')
            .fromTo($('#scene-1 #Trees_1_'), 2, {y: 1000}, {y: 0, ease: Power1.easeInOut}, '-=1.8')
            .fromTo($('#scene-1 #Ground'), 2, {y: 1000}, {y: 0, ease: Power1.easeInOut}, '-=1.8')
*/
            e.preventDefault();
            e.stopPropagation();

        },
        handleScene2Link: function(e){
            $('.scene').fadeOut(1000);
            $('#scene-2').show();
            $('#start-over-scene2').fadeIn(1000);

            var scene2Linktl = new TimelineMax();

            scene2Linktl.fromTo($('#scene-2 #Cloud_1_'), 2, {y: 1000} , {y: 0, ease: Power1.easeInOut})
            .fromTo($('#scene-2 #Buildings'), 2, {y: 1000}, {y: 0, ease: Power1.easeInOut}, '-=1.8')
            .fromTo($('#scene-2 #scene-2-clickpoint'), 2, {y: 1000}, {y: 0, ease: Power1.easeInOut}, '-=1.8')
            .fromTo($('#scene-2 #scene-2-dotted-line'), 2, {y: 1000}, {y: 0, ease: Power1.easeInOut}, '-=1.8')
            .fromTo($('#scene-2 #Trees_1_'), 2, {y: 1000}, {y: 0, ease: Power1.easeInOut}, '-=1.8')
            .fromTo($('#scene-2 #Ground'), 2, {y: 1000}, {y: 0, ease: Power1.easeInOut}, '-=1.8')
            .fromTo($('#scene-2 #Bill'), 2, {y: 1000}, {y: 0, ease: Power1.easeInOut,}, '-=1.8')
            .fromTo($('#scene-2 #Guy_x5F_on_x5F_SkateBoard'), 2, {x: -1000},{x: 0, ease: Power1.easeInOut}, '-=1.8')

            e.preventDefault();
            e.stopPropagation();
  
        },
        handleScene3Link: function(e){
            $('.scene').fadeOut(1000);
            $('#scene-3').show();
            $('#start-over-scene3').fadeIn(1000);

            var scene3Linktl = new TimelineMax();

            scene3Linktl.fromTo($('#scene-3 #BG_x5F_building'), 2, {y: 1000}, {y: 0, ease: Power1.easeInOut})
            .fromTo($('#scene-3 #scene-3-clickpoint'), 2, {y: 1000}, {y: 0, ease: Power1.easeInOut}, '-=1.8')
            .fromTo($('#scene-3 #scene-3-dotted-line'), 2, {y: 1000}, {y: 0, ease: Power1.easeInOut}, '-=1.8')
            .fromTo($('#scene-3 #Land'), 2, {y: 1000}, {y: 0, ease: Power1.easeInOut}, '-=1.8')
            .fromTo($('#scene-3 #Bus_1_'), 2, {y: 1000}, {y: 0, ease: Power1.easeInOut}, '-=1.8')
            .fromTo($('#scene-3 #BusStop_1_'), 2, {y: 1000}, {y: 0, ease: Power1.easeInOut}, '-=1.8')
            .fromTo($('#scene-3 #Bill_1_'), 2, {y: 1000}, {y: 0, ease: Power1.easeInOut,}, '-=1.8')
            .fromTo($('#scene-3 #Couple_1_'), 2, {x: 1000},{x: 0, ease: Power1.easeInOut}, '-=1.8')

            e.preventDefault();
            e.stopPropagation();
            
        },
        handleScene4Link: function(e){
            $('.scene').fadeOut(1000);
            $('#scene-4').show();
            $('#start-over-scene4').fadeIn(1000);

            var scene4Linktl = new TimelineMax();
            
            scene4Linktl.fromTo($('#scene-4 #BusSide'), 2, {y: 1000}, {y: 0, ease: Power1.easeInOut})
            .fromTo($('#scene-4 #scene-4-clickpoint'), 2, {y: 1000}, {y: 0, ease: Power1.easeInOut}, '-=1.8')
            .fromTo($('#scene-4 #scene-4-dotted-line'), 2, {y: 1000}, {y: 0, ease: Power1.easeInOut}, '-=1.8')    
            .fromTo($('#scene-4 #BusSeatLeft'), 2, {y: 1000}, {y: 0, ease: Power1.easeInOut}, '-=1.8')
            .fromTo($('#scene-4 #LadySitting'), 2, {y: 1000}, {y: 0, ease: Power1.easeInOut}, '-=1.8')            
            .fromTo($('#scene-4 #BusSeatRight'), 2, {y: 1000}, {y: 0, ease: Power1.easeInOut}, '-=1.8')
            .fromTo($('#scene-4 #Bill_1_'), 2, {y: 1000}, {y: 0, ease: Power1.easeInOut,}, '-=1.8')
            .fromTo($('#scene-4 #WorkOutGirl'), 2, {x: 1000},{x: 0, ease: Power1.easeInOut}, '-=1.8')

            e.preventDefault();
            e.stopPropagation();
        },
        handleScene5Link: function(e){
            $('.scene').fadeOut(1000);
            $('#scene-5').show();
            $('#start-over-scene5').fadeIn(1000);
            
            var scene5Linktl = new TimelineMax();

            scene5Linktl.fromTo($('#scene-5 #Light_1_'), 2, {y: 1000}, {y: 0, ease: Power1.easeInOut})
            .fromTo($('#scene-5 #scene-5-clickpoint'), 2, {y: 1000}, {y: 0, ease: Power1.easeInOut}, '-=1.8')
            .fromTo($('#scene-5 #scene-5-dotted-line'), 2, {y: 1000}, {y: 0, ease: Power1.easeInOut}, '-=1.8')    
            .fromTo($('#scene-5 #Shelf'), 2, {y: 1000}, {y: 0, ease: Power1.easeInOut}, '-=1.8')
            .fromTo($('#scene-5 #Tables'), 2, {y: 1000}, {y: 0, ease: Power1.easeInOut}, '-=1.8')     
            .fromTo($('#scene-5 #Suitcase_1_'), 2, {y: 1000}, {y: 0, ease: Power1.easeInOut}, '-=1.8')
            .fromTo($('#scene-5 #Couch_1_'), 2, {y: 1000}, {y: 0, ease: Power1.easeInOut,}, '-=1.8')

            e.preventDefault();
            e.stopPropagation();
        },
        handleScene6Link: function(e){
            $('.scene').fadeOut(1000);
            $('#scene-6').show();
            $('#start-over-scene6').fadeIn(1000);
            
            var scene6Linktl = new TimelineMax();

            scene6Linktl.fromTo($('#scene-6 #TV_x5F_stand'), 2, {y: 1000}, {y: 0, ease: Power1.easeInOut})
            .fromTo($('#scene-6 #scene-6-clickpoint'), 2, {y: 1000}, {y: 0, ease: Power1.easeInOut}, '-=1.8')
            .fromTo($('#scene-6 #scene-6-dotted-line'), 2, {y: 1000}, {y: 0, ease: Power1.easeInOut}, '-=1.8')
            .fromTo($('#scene-6 #Hand_x5F_CellPhone'), 2, {y: 1000}, {y: 0, ease: Power1.easeInOut}, '-=1.8')    
            .fromTo($('#scene-6 #Couch_1_'), 2, {y: 1000}, {y: 0, ease: Power1.easeInOut}, '-=1.8')
            .fromTo($('#scene-6 #Bill'), 2, {y: 1000}, {y: 0, ease: Power1.easeInOut}, '-=1.8')
            .fromTo($('#scene-6 #Suitcase_1_'), 2, {y: 1000}, {y: 0, ease: Power1.easeInOut}, '-=1.8')

            e.preventDefault();
            e.stopPropagation();
        },
        handleScene7Link: function(e){
            $('.scene').fadeOut(1000);
            $('#scene-7').show();
            $('#start-over-scene7').fadeIn(1000);
            
            var scene7Linktl = new TimelineMax();

            scene7Linktl.fromTo($('#scene-7 #Hand_x5F_CellPhone'), 2, {y: 1000}, {y: 0, ease: Power1.easeInOut})           
            .fromTo($('#scene-7 #BottomLine_1_'), 2, {y: 1000}, {y: 0, ease: Power1.easeInOut}, '-=1.8')

            TweenLite.fromTo($('#scene-7 #scene-7-clickpoint'), 2.1, {x: 1000}, {x: 10, ease: Power1.easeInOut})
            TweenLite.fromTo($('#scene-7 #scene-7-dotted-line'), 2.1, {x: 1000}, {x: 8, ease: Power1.easeInOut}) 

            e.preventDefault();
            e.stopPropagation();
        },
        handleScene8Link: function(e){
            $('.scene').fadeOut(1000);
            $('#scene-8').show();
            $('#start-over-scene8').fadeIn(1000);
            
            var scene8Linktl = new TimelineMax();

            scene8Linktl.fromTo($('#scene-8 #Lights'), 2, {y: 1000}, {y: 0, ease: Power1.easeInOut})
            .fromTo($('#scene-8 #scene-8-clickpoint'), 2, {y: 1000}, {y: 0, ease: Power1.easeInOut}, '-=1.8')
            .fromTo($('#scene-8 #scene-8-dotted-line'), 2, {y: 1000}, {y: 0, ease: Power1.easeInOut}, '-=1.8')    
            .fromTo($('#scene-8 #Land'), 2, {y: 1000}, {y: 0, ease: Power1.easeInOut}, '-=1.8')
            .fromTo($('#scene-8 #Floor'), 2, {y: 1000}, {y: 0, ease: Power1.easeInOut}, '-=1.8')
            .fromTo($('#scene-8 #TableLegs'), 2, {y: 1000}, {y: 0, ease: Power1.easeInOut}, '-=1.8')
            .fromTo($('#scene-8 #Left_x5F_Window'), 2, {y: 1000}, {y: 0, ease: Power1.easeInOut}, '-=1.8')
            .fromTo($('#scene-8 #Left_x5F_TopTable'), 2, {y: 1000}, {y: 0, ease: Power1.easeInOut}, '-=1.8')
            .fromTo($('#scene-8 #Left_x5F_Bill'), 2, {y: 1000}, {y: 0, ease: Power1.easeInOut}, '-=1.8')
            .fromTo($('#scene-8 #Suitcase'), 2, {y: 1000}, {y: 0, ease: Power1.easeInOut}, '-=1.8')
            .fromTo($('#scene-8 #RightWindow'), 2, {y: 1000}, {y: 0, ease: Power1.easeInOut}, '-=1.8')
            .fromTo($('#scene-8 #Right_x5F_Hacker_x5F_01'), 2, {y: 1000}, {y: 0, ease: Power1.easeInOut,}, '-=1.8')
            .fromTo($('#scene-8 #Right_x5F_TopTable'), 2, {y: 1000}, {y: 0, ease: Power1.easeInOut}, '-=1.8')
            .fromTo($('#scene-8 #GFX_x5F_on_x5F_Screen_x5F_01'), 2, {y: 1000}, {y: 0, ease: Power1.easeInOut}, '-=1.8')

            e.preventDefault();
            e.stopPropagation();
        },
        handleScene9Link: function(e){
            $('.scene').fadeOut(1000);
            $('#scene-9').show();
            $('#start-over-scene9').fadeIn(1000);

            var scene9LinkTL = new TimelineMax();

            scene9LinkTL.fromTo($('#scene-9 #Light'), 2, {y: 1000}, {y: 0, ease: Power1.easeInOut})    
            .fromTo($('#scene-9 #Ground'), 2, {y: 1000}, {y: 0, ease: Power1.easeInOut}, '-=1.8')
            .fromTo($('#scene-9 #Table'), 2, {y: 1000}, {y: 0, ease: Power1.easeInOut}, '-=1.8')
            .fromTo($('#scene-9 #Bill'), 2, {y: 1000}, {y: 0, ease: Power1.easeInOut}, '-=1.8')
            .fromTo($('#scene-9 #RightHacker_1_'), 2, {y: 1000}, {y: 0, ease: Power1.easeInOut}, '-=1.8')
            .fromTo($('#scene-9 #Wall_1_'), 2, {y: 1000}, {y: 0, ease: Power1.easeInOut}, '-=1.8')
            .fromTo($('#scene-9 #scene-9-clickpoint'), 2, {y: 1000}, {y: 0, ease: Power1.easeInOut}, '-=1.8')
            .fromTo($('#scene-9 #scene-9-dotted-line'), 2, {y: 1000}, {y: 0, ease: Power1.easeInOut}, '-=1.8')

            e.preventDefault();
            e.stopPropagation();
        },
        
        handleStartOverBtn: function(e){
            app.handleScene1Link();

            e.preventDefault();
            e.stopPropagation();
        }
        
    };

    $( document ).ready(function(){
        app.init();
        $('#page_fade').fadeIn(1000);

    });

})(jQuery, IBMCore);
