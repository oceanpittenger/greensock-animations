
## Overview

This repository was a project created at Centerline for IBM 

## Getting Started

After cloning/copying this repository, you should only need to run `npm install` in your terminal to get started.

## Folder Structure

```
public/
  |  assets/
  |  |  stylesheets/
  |  |  |  style.css
  |  |  javascript/
  |  |  |  vendor/
  |  |  |  |  something.min.js
  |  |  |  scripts.js
  |  index.html
source/
  |  javascript/
  |  |  a.js
  |  |  b.js
  |  |  c.js
  |  scss/
  |  |  styles.scss
  |  |  other-things.scss
gulpfile.js
index.html
package.json

```

`/source` is the folder where we will do our work. `/public` is the folder where the pages will run from.  The `/public/assets` folder houses all of the images, compiled javascript files, stylesheets, and any other media.

Plugins and third-party libraries will live in `public/assets/javascript/vendor`, and *will not* be minified or concatenated into our scripts.js file.

`index.html` will automatically perform a meta-refresh to the `public/` folder.



## Basic Toolbox

### NPM

This repo makes use of NPM.  To install dev dependencies, run `npm install` in a terminal.  Additionally, when cloning this repository, please update the information in the `package.json` file.

### Gulp

The current build tool is Gulp.  The `gulpfile.js` contains all of the tasks that you can run.  Feel free to modify them and add more if needed.

The **default** task is for running while developing locally.  It will automatically compile sass and babel code into the appropriate `/assets` folders.  It will also update your documentation for you.

The **build** task is essentially the same as the default task, but without the watching and browser syncing.  It should be run before creating a release package to ensure the compiled and minified versions of the code is up to date.

## Editor Plugins


### Sublime Text
 - [editorconfig] (https://github.com/sindresorhus/editorconfig-sublime#readme)
 - [JSHint Gutter](https://packagecontrol.io/packages/JSHint%20Gutter) or [SublimeLinter](http://www.sublimelinter.com/en/latest/)
 - [Javascript Next](https://packagecontrol.io/packages/JavaScriptNext%20-%20ES6%20Syntax)
 - [Gulp](https://packagecontrol.io/packages/Gulp)
